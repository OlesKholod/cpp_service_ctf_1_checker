import subprocess
# import check1
import flag_generator
import os

def main():
    print('TEST')
    cmd = './check1.py check 127.0.0.1'
    subprocess.run(cmd, shell=True)
    flag1 = flag_generator.flag_gen() + 'm'
    cmd = f'./check1.py put 127.0.0.1 1 {flag1} 1'
    print(cmd)
    subprocess.run(cmd, shell=True)
    flag2 = flag_generator.flag_gen() + 'm'
    cmd = f'./check1.py put 127.0.0.1 1 {flag2} 2'
    print(cmd)
    subprocess.run(cmd, shell=True)
    cmd = f'./check1.py get 127.0.0.1 1 {flag1} 1'
    print(cmd)
    subprocess.run(cmd, shell=True)
    cmd = f'./check1.py get 127.0.0.1 1 {flag2} 2'
    print(cmd)
    subprocess.run(cmd, shell=True)

    cmd = './check1.py info'
    subprocess.run(cmd, shell=True)

    print("END")


if __name__ == '__main__':
    main()