import requests
import sys
import random
import string
from pwn import *
from enum import Enum
from sys import argv

random = random.SystemRandom()
PORT = 8889
EXPLOIT_NAME = argv[0]

class user_data():
    def __init__(self, login, password, key_phrase):
        self.login = login
        self.password = password
        self.key_phrase = key_phrase

    @classmethod
    def from_str(cls, string):
        data = string.split('\t')
        data_key_value = data[0].split(': ')
        login = data_key_value[1]
        data_key_value = data[1].split(': ')
        password = data_key_value[1]
        data_key_value = data[2].split(': ')
        key_phrase = data_key_value[1]
        return cls(login, password, key_phrase)

    def __str__(self):
        string = "USER_DATA OBJECT\n"
        string += "login: " + self.login + "\npassword: " + self.password + "\nkey_phrase: " + self.key_phrase
        return string

    def __repr__(self):
        string = "USER_DATA OBJECT\n"
        string += "login: " + self.login + "\npassword: " + self.password + "\nkey_phrase: " + self.key_phrase
        return string

class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110

class CheckSession():
    LL = 1000

def check(host: str):
    login = _get_login()
    password = _get_password()
    key_phrase = _get_key_phrase()
    _register(host, port, login, password, key_phrase)
    _log(f"Going to save secret '{name}'")

    #f
    pass

def put(host: str, flag_id: str, flag: str, vuln: str):
    conn = remote(host, PORT)
    data = conn.recvuntil(">>\n")
    print(data)
    conn.sendline('1\n')
    data = conn.recvuntil(">>\n")
    print(data)
    conn.sendline()
    pass

def get(host: str, flag_id: str, flag: str, vuln: str):
    pass

def info():
    pass

def flag_gen():
    #random.seed("flags_for_ctf")
    team_number = random.randint(1, 9)
    team_uuid = ''.join(random.choices(string.ascii_uppercase + string.digits, k=32))
    generated_string = f"TEAM00{team_number}_{team_uuid}"
    return generated_string

def example_1(host, USER:user_data):
    #register
    conn = remote(host, PORT)
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(b'1\n')
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(USER.login.encode())
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(USER.password.encode())
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(USER.key_phrase.encode())
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(b'4\n')
    data = conn.recvuntil(b"\n")
    print(data)
    conn.close()

def example_2(host, USER:user_data, new_key):
    #login and change key_phrase
    conn = remote(host, PORT)
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(b'2\n')
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(USER.login.encode())
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(USER.password.encode())
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(b'5\n')
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(new_key.encode())
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(b'8\n')
    data = conn.recvuntil(b"\n")
    print(data)
    conn.close()

def example_3(host, USER:user_data, new_flag):
    #login and solve challenge
    conn = remote(host, PORT)
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'2')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(USER.login.encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(USER.password.encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'1')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'0')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'1')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    #print(new_flag.encode())
    conn.sendline(new_flag.encode())
    #conn.sendline(b'TEAM006_RVXEHNKSM9T9JG47MQ8QBUHYEKMF7G8D')
    data = conn.recvuntil(b">>\n")
    print(data)
    conn.sendline(b'0')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'8')
    data = conn.recvuntil(b"\n")
    print(data.decode())
    conn.close()

def exp_test1():
    USERS = []
    list_data = []
    with open('my_file.txt', 'r') as file:
        for line in file:
            list_data.append(line.strip())
    
    for x in range(0, len(list_data) // 3):
        data = list_data[x * 3] + '\t' + list_data[x * 3 + 1] + '\t' + list_data[x * 3 + 2]
        U = user_data.from_str(data)
        USERS.append(U)
    
    list_data.clear()
    for U in USERS:
        example_1("127.0.0.1", U)


def exp_test2():
    pass

def exp_test3():
    USERS = []
    list_data = []
    with open('my_file.txt', 'r') as file:
        for line in file:
            list_data.append(line.strip())
    
    for x in range(0, len(list_data) // 3):
        data = list_data[x * 3] + '\t' + list_data[x * 3 + 1] + '\t' + list_data[x * 3 + 2]
        U = user_data.from_str(data)
        USERS.append(U)
    
    list_data.clear()
    for U in USERS:
        example_3("127.0.0.1", U, flag_gen())
    


def main():
    print("CHECKER")
    try:
        cmd = argv[1]
        hostname = argv[2]
        if cmd == "get":
            fid, flag = argv[3], argv[4]
            get(hostname, fid, flag)
        elif cmd == "put":
            fid, flag = argv[3], argv[4]
            put(hostname, fid, flag)
        elif cmd == "check":
            check(hostname)
        elif cmd == "info":
            info()
        else:
            raise IndexError
    except IndexError:
        die(
            ExitStatus.CHECKER_ERROR,
            f"Usage: {argv[0]} check|put|get IP FLAGID FLAG",
        )

if __name__ == "__main__":
    #main()

    #exp_test3()

    USER = user_data("0mE08DolwZp1JZ9Wkq", "Gk_PGQq-p-nWttRzlw", "TEAM006_RVXEHNKSM9T9JG47MQ8QBUHYEKMF7G8D")
    # #example_2("127.0.0.1", USER, flag_gen())
    example_3("127.0.0.1", USER, flag_gen())